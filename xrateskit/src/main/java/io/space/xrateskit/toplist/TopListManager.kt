package io.space.xrateskit.toplist

import io.space.xrateskit.api.CryptoCompareProvider
import io.space.xrateskit.entities.PriceInfo
import io.reactivex.Single

class TopListManager(private val provider: CryptoCompareProvider) {

    fun getTopList(currency: String): Single<List<PriceInfo>> {
        return provider.getTopListCoins(currency)
            .flatMap { coinCodes ->
                provider.getPriceInfo(coinCodes, currency)
            }
    }

}
