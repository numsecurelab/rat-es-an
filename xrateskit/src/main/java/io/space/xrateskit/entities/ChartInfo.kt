package io.space.xrateskit.entities

class ChartInfo(
        val points: List<ChartPoint>,
        val startTimestamp: Long,
        val endTimestamp: Long
)
