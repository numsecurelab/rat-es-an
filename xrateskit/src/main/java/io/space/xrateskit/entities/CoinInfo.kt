package io.space.xrateskit.entities


class CoinInfo(
        val coinCode: String,
        val coinName: String
)
