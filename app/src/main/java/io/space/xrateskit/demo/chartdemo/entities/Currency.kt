package io.space.xrateskit.demo.chartdemo.entities

data class Currency(val code: String, val symbol: String)
